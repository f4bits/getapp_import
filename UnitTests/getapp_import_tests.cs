﻿using System;
using System.IO;
using System.Linq;
using getapp_import;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;


namespace UnitTests
{
    [TestClass]
    public class getapp_import_tests
    {
        private const string TestDocument = @"---
            products:
            -
              tags: ""Bugs & Issue Tracking, Development Tools""
              name: ""GitHub""
              twitter: ""github""
            -
              tags: ""Instant Messaging & Chat,Web Collaboration,Productivity""
              name: ""Slack""
              twitter: ""slackhq""
            -
              tags: ""Project Management,Project Collaboration,Development Tools""
              name: ""JIRA Software""
              twitter: ""jira""
                    ";

        [TestMethod]
        public void DeserializeFile_Test()
        {
            var input = new StringReader(TestDocument);


            var deserializer = new DeserializerBuilder()
                .WithNamingConvention(new CamelCaseNamingConvention())
                .Build();

            var provider = deserializer.Deserialize<Provider>(input);

            Assert.IsTrue(provider.Products.Any());
        }
    }
}
